package com.example.checkassitance

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.widget.Button
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.widget.Toast
import java.net.URL
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        val current = LocalDateTime.now()
        val formatter = DateTimeFormatter.BASIC_ISO_DATE
        val formatted = current.format(formatter)
        val vacio = ""
        var letra = formatted.split(vacio)
        var fecha = letra[6]+letra[7]+"-"+letra[5]+"-"+letra[0]+letra[1]+letra[2]+letra[3]
        var btnBlue: Button = findViewById(R.id.button)
        var btnCheck: Button = findViewById(R.id.button2)

        btnBlue.setOnClickListener {
            var Blue: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

            val discovery = Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE)
            startActivityForResult(
                discovery, 0
            )
            Toast.makeText( applicationContext, "On bluetooth",Toast.LENGTH_LONG).show()
        }

        btnCheck.setOnClickListener {

            try {
                URL("https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/"+fecha+"/students/201318878").openStream().bufferedReader().use { it.readText() }
                Toast.makeText(applicationContext, "se pudo :D", Toast.LENGTH_LONG).show()
            }
            catch(e: Exception) {
                Toast.makeText(applicationContext, "no :( estas", Toast.LENGTH_LONG).show()
            }
        }
    }
}
